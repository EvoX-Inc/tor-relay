# Tor Relay

![Tor Logo](https://fra.privateinternetaccess.com/assets/tor-logo-large-89ac1c118b86d69953fff1ab31128550fcce9a74162e9b3fb7d0fc4bfd83e1ed.png)

This Script aim to help Tor Network to grow !

**It can create :**

    Middle Relay (Recommended)
    Bridge Relay
    Exit Relay (Only Advanced User)

**Distro Supported :**

    Debian
    Other (Debian Based)

When Relay was created you can forgot and let run it.

**Update and upgrade will be automatic with unattended-upgrades**

## Host Providers

**BlackHost** | https://black.host/


## Relay Requirements

**Bandwith** 
    
    ~100Mbps or more

**Montly Outbound Traffic** 
    
    5 TB/Month | Best is unmetered plan

**Memory** 

    Non-Exit Relay with less than 40Mbit/s need 512MB of RAM
    Non-Exit Relay faster than 40 Mbit/s need 1GB or more
    Exit Relay need 1.5GB or more

**Storage** 
    
    200MB is good

**CPU** 
    
    AES-NI Support would be great for better network performance
