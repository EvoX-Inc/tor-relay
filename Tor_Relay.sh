#!/bin/bash
####################################################################################################################
### This Script aim to create Middle Relay, Exit Relay or Bridge on Tor Network to help to grown the Tor Project ###
####################################################################################################################

## Variable def
CHOICE=0
COLOR_RED="\e[31m"
COLOR_GREEN="\e[32m"
COLOR_DEFAULT="\e[39m"
RELEASE=$(lsb_release -c -s)

## Function
function Success() {
  echo -e "${COLOR_GREEN}$1${COLOR_DEFAULT}"
}

function handleError() {
  Error "Fail"
  systemctl stop tor
  Error "An error occured on the last setup step."
}

function Error() {
  echo -e "${COLOR_RED}$1${COLOR_DEFAULT}"
}

## Check Root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

## Set MOTD
cat << EOF > /etc/motd

 ████████╗ ██████╗ ██████╗     ██████╗ ███████╗██╗      █████╗ ██╗   ██╗
 ╚══██╔══╝██╔═══██╗██╔══██╗    ██╔══██╗██╔════╝██║     ██╔══██╗╚██╗ ██╔╝
    ██║   ██║   ██║██████╔╝    ██████╔╝█████╗  ██║     ███████║ ╚████╔╝
    ██║   ██║   ██║██╔══██╗    ██╔══██╗██╔══╝  ██║     ██╔══██║  ╚██╔╝
    ██║   ╚██████╔╝██║  ██║    ██║  ██║███████╗███████╗██║  ██║   ██║
    ╚═╝    ╚═════╝ ╚═╝  ╚═╝    ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝   ╚═╝

EOF

## Configure DNS
cp /etc/resolv.conf /etc/resolv.conf.backup
echo "nameserver 193.17.47.1" > /etc/resolv.conf
echo "nameserver 185.43.135.1" >> /etc/resolv.conf
chattr +i /etc/resolv.conf

## Repository
echo "deb http://deb.debian.org/debian buster main" > /etc/apt/sources.list
echo "deb-src http://deb.debian.org/debian buster main" >> /etc/apt/sources.list
echo ""  >> /etc/apt/sources.list
echo "deb http://security.debian.org/ buster/updates main" >> /etc/apt/sources.list
echo "deb-src http://security.debian.org/ buster/updates main" >> /etc/apt/sources.list
echo "deb http://deb.debian.org/debian buster-updates main" >> /etc/apt/sources.list
echo "deb-src http://deb.debian.org/debian buster-updates main" >> /etc/apt/sources.list
echo ""  >> /etc/apt/sources.list
echo "deb http://deb.debian.org/debian buster-backports main" >> /etc/apt/sources.list
echo "deb-src http://deb.debian.org/debian buster-backports main" >> /etc/apt/sources.list


echo "Adding Tor repository..."
touch /etc/apt/sources.list.d/tor.list && Success "  Successfully Created Tor List" || handleError
echo "deb https://deb.torproject.org/torproject.org $RELEASE main" > /etc/apt/sources.list.d/tor.list && Success "  DEB Successfully Writed" || handleError
echo "deb-src https://deb.torproject.org/torproject.org $RELEASE main" >> /etc/apt/sources.list.d/tor.list && Success "  DEB-SRC Successfully Writed" || handleError

echo "Adding Tor GPG key..."
curl -s https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | apt-key add - && Success "  Successful" || handleError

## Update Machine
echo "Installing Update..."
apt-get -y update > /dev/null && apt-get -y full-upgrade > /dev/null && apt-get -y dist-upgrade > /dev/null && apt-get -y autoremove > /dev/null && apt-get -y autoclean > /dev/null && Success "  Successful" || handleError

## Install Dependencies
echo "Installing Tools..."
apt-get -y install lsb-release python3-pip python3-distutils fail2ban unattended-upgrades apt-listchanges apt-transport-https psmisc dirmngr curl  > /dev/null && Success "  Successful" || handleError

## Install Tor
echo "Installing Tor..."
apt-get -y install tor > /dev/null && Success "  Successful" || handleError

## Install Tools
  echo "Installing NYX..."
  NYX_INSTALLED=false
  apt-get -y install nyx  > /dev/null && NYX_INSTALL_OK=true && Success "  Successful" || Error "Failed to install NYX via Aptitude trying PIP !"

  if [[ ! NYX_INSTALLED ]];
  then
    echo "Installing via PIP..."
    pip3 install nyx > /dev/null && Success "  Successful" || Error "NYX not Installed because Fail with 2 method !"
  fi

  ## Unattended Upgrade
  echo "APT::Periodic::Update-Package-Lists "1";" > /etc/apt/apt.conf.d/20auto-upgrades
  echo "APT::Periodic::Unattended-Upgrade "1";" >> /etc/apt/apt.conf.d/20auto-upgrades
  systemctl enable unattended-upgrades > /dev/null && Success "  Successful"
  systemctl restart unattended-upgrades > /dev/null && Success "  Successful"

## Choose Type of Relay
echo -n "Type of Relay (Middle = 0, Bridge = 1 or Exit = 2) : "; read CHOICE

if [[ $CHOICE -eq 0 ]];then
  ## Configure Tor Middle Relay
  export NAME=RelayName
  export EMAIL=john.doe@protonmail.ch

  echo -n "Name of Relay : "; read NAME

  echo -n "Email of Relay : "; read EMAIL

  echo "Nickname $NAME" > /etc/tor/torrc
  echo "ORPort 443" >> /etc/tor/torrc
  echo "ExitRelay 0" >> /etc/tor/torrc
  echo "SocksPort 0" >> /etc/tor/torrc
  echo "ExitPolicy reject *:*" >> /etc/tor/torrc
  echo "ContactInfo $EMAIL" >> /etc/tor/torrc
fi

if [[ $CHOICE -eq 1 ]]; then
  ## Installing Obfs4Proxy
  echo "Installing Obfs4Proxy..."
  apt-get -y install obfs4proxy > /dev/null && Success "  Successful" || handleError

  ## Configure Tor Bridge Relay
  export NAME=RelayName
  export EMAIL=john.doe@protonmail.ch

  echo -n "Name of Relay : "; read NAME

  echo -n "Email of Relay : "; read EMAIL

  echo "BridgeRelay 1" > /etc/tor/torrc
  echo "ORPort 443" >> /etc/tor/torrc
  echo "ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy" >> /etc/tor/torrc
  echo "ServerTransportListenAddr obfs4 0.0.0.0:8042" >> /etc/tor/torrc
  echo "ExtORPort auto" >> /etc/tor/torrc
  echo "ContactInfo $EMAIL" >> /etc/tor/torrc
  echo "Nickname $NAME" >> /etc/tor/torrc
fi

if [[ $CHOICE -eq 2 ]]; then
  ## Configure Tor Exit Relay
  echo "DirPort 80" > /etc/tor/torrc
  echo "ExitRelay 1" >> /etc/tor/torrc
  echo "ExitPolicy accept *:53        # DNS" >> /etc/tor/torrc
  echo "ExitPolicy accept *:80        # HTTP" >> /etc/tor/torrc
  echo "ExitPolicy accept *:443       # HTTPS" >> /etc/tor/torrc
  echo "ExitPolicy reject6 *:*, reject *:*" >> /etc/tor/torrc
fi

## Indicate if running other Relay
export ORELAY=0

echo -n "Are you running other relay (No = 0, Yes = 1) : "; read ORELAY

## Relay Family
if [[ $ORELAY -eq 1 ]]; then
  export FINRELAY=ABCDE12345

  echo -n "Other Relay Fingerprint (separated with commas) : "; read FINRELAY

  echo "MyFamily $FINRELAY" >> /etc/tor/torrc
fi

## Launch NYX at Restart
export NYXSTART=0

echo -n "Do you want start NYX when you login (No = 0, Yes = 1) : "; read NYXSTART

if [[ $NYXSTART -eq 1 ]]; then
  echo "sudo -u debian-tor nyx" >> ~/.bashrc
fi
    ## Restart Tor Service
    systemctl restart tor@default

    ## Clear Output
    clear

    ## Done
    echo "Thank you for your support to Tor Network !"
    sleep 2
    clear
    echo "Rebooting Server in 3"
    sleep 1
    clear
    echo "Rebooting Server in 2"
    sleep 1
    clear
    echo "Rebooting Server in 1"
    sleep 1
    clear
    echo "Rebooting..."
    reboot
